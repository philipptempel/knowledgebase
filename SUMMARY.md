# Table of contents

* [Introduction](README.md)

## ✍ LaTeX

* [Introduction](latex/introduction.md)
* [Tables](latex/tables.md)

## 👨🔧 Mechanics

* [Introduction](mechanics/introduction.md)

## 🤖 Robotics

* [Introduction](robotics/introduction.md)

## 🎓 Academia

* [Introduction](academia/introduction.md)
