---
description: Everything you need to know about beautiful and camera-ready tables in LaTeX
---

# Tables

These links are taken from [https://tex.stackexchange.com/a/112382/64579](https://tex.stackexchange.com/a/112382/64579)

* [http://www.inf.ethz.ch/personal/markusp/teaching/guides/guide-tables.pdf](http://www.inf.ethz.ch/personal/markusp/teaching/guides/guide-tables.pdf)
* [http://cpansearch.perl.org/src/LIMAONE/LaTeX-Table-v1.0.6/examples/examples.pdf](http://cpansearch.perl.org/src/LIMAONE/LaTeX-Table-v1.0.6/examples/examples.pdf)
* [http://www.tug.org.in/tutorial/chap08-scr.pdf](http://www.tug.org.in/tutorial/chap08-scr.pdf)
